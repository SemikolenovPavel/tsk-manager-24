package ru.t1.semikolenov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IAuthService getAuthService();

}
