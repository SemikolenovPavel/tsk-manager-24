package ru.t1.semikolenov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class AboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Pavel Semikolenov");
        System.out.println("E-mail: pavelsemikolenov@yandex.ru");
    }

}
