package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.repository.IProjectRepository;
import ru.t1.semikolenov.tm.api.service.IProjectService;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.exception.entity.ModelNotFoundException;
import ru.t1.semikolenov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.semikolenov.tm.exception.field.*;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.repository.ProjectRepository;

import java.util.Date;
import java.util.Optional;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull IProjectRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = repository.create(userId, name);
        return project;
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = repository.create(userId, name, description);
        return project;
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull String name,
            @NotNull String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Project project = create(name, description);
        if (dateBegin == null) throw new IncorrectDateBeginException();
        else project.setDateBegin(dateBegin);
        if (dateEnd == null) throw new IncorrectDateEndException();
        else project.setDateEnd(dateEnd);
        project.setUserId(userId);
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        if (dateBegin == null) throw new IncorrectDateBeginException();
        else project.setDateBegin(dateBegin);
        if (dateEnd == null) throw new IncorrectDateEndException();
        else project.setDateEnd(dateEnd);
        project.setUserId(userId);
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IncorrectIndexException();
        if (index >= repository.getSize()) throw new IncorrectIndexException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @NotNull final String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IncorrectIndexException();
        if (index >= repository.getSize()) throw new IncorrectIndexException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        if (dateBegin == null) throw new IncorrectDateBeginException();
        else project.setDateBegin(dateBegin);
        if (dateEnd == null) throw new IncorrectDateEndException();
        else project.setDateEnd(dateEnd);
        project.setUserId(userId);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @NotNull final String userId,
            @NotNull String id,
            @NotNull Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Project project = findOneById(userId, id);
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull Status status) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IncorrectIndexException();
        if (index >= repository.getSize()) throw new IncorrectIndexException();
        @NotNull final Project project = findOneByIndex(userId, index);
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

}
