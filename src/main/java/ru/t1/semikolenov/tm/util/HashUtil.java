package ru.t1.semikolenov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    String SECRET = "1245654849";

    @NotNull
    Integer ITERATION = 32153;

    @NotNull
    static String salt(@NotNull final String value) {
        @NotNull String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    @NotNull
    static String md5(@NotNull String value) {
        try {
            @NotNull java.security.MessageDigest md =
                    java.security.MessageDigest.getInstance("MD5");
            @NotNull byte[] array = md.digest(value.getBytes());
            @NotNull StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100))
                        .substring(1, 3);
            }
            return sb.toString();
        } catch (@NotNull NoSuchAlgorithmException e) {
            throw new RuntimeException();
        }
    }

}
