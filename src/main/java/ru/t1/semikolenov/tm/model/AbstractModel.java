package ru.t1.semikolenov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractModel {

    protected String id = UUID.randomUUID().toString();

}
